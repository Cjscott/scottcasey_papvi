# My project's README

App Name: WantsApp

This application requires the use of the internet for loading and saving data to the real time database.

Known Bugs:
Messaging feature will crash the app after a little bit of using it.

Missing functionality:
The group list viewing feature was not implemented.


Known Users For Testing:

test@ymail.com  - Password = testuser Phone# 4077898233  Username = TestUser1234 Fullname = test user


test2@ymail.com - Password = testuser  Phone# 4077898255 Username = TestUseR4321  Fullname = test2 user


ExampleUser@email.com - Password = examplepass  Phone# 5555555555 Username = exampleusername Fullname = example user
